# UpstreamProject

## Start a new club website

1. Create an issue in StudentGovernment to request a website. Include
    * Club name
    * GitLab usernames for the members who will build and maintain the website.
    * Agree to use our UpstreamTemplate.
    The Student Government Webmaster will create a GitLab subgroup for your club and add the members with appropriate permissions to the subgroup. After that is done...
2. Fork the [UpstreamTemplate](https://gitlab.com/hfossedu/web-template-example/upstreamtemplate) project to your subgroup.
3. Remove the fork relationship. LINK
4. Determine your site's GitLab pages URL. LINK
5. Edit your project's description LINK and set it to your site's URL.
6. Follow "Updating your club's files" below to modify the following in your club's project:
    - Edit `_config.yml` and set your club's name and description.
    - Fill in content for each page found in the `_pages/` directory.
7. Create an issue LINK in [StudentGovernment](https://gitlab.com/hfossedu/web-template-example/studentgovernment/student-government) to add a link to your club's site, providing a link to your GitLab project.

Do not change any other files. Doing so will make it difficult to upgrade your clubs repository with new changes in the UpstreamTemplate. If you need think something needs to bechanged in another file, follow the instructions in [Contributing a change to the UpstreamTemplate]().

## Updating your club's files

1. Create an issue in your project to propose your change. This allows others to discuss and debate it, and ultimately form a common understanding of the change to be made.
2. From the issue create a merge request. Be sure to ***target the main branch of your fork*** and not the upstream repository. Enable "Delete the source branch ..." and "Squash commits ...".
3. From the merge request, open the project in the Web IDE.
4. Make, test, and commit your changes.
5. From the merge request, click "View app" to preview your changes. (Wait for all pipeline jobs to finish running. It may take several minutes. Periodically refresh the page until it's done.)
6. When satisfied, mark the merge request as "ready", and merge it.
7. Wait until the pipeline for the main branch is complete (refresh regularly) and then view your updated site.

## If StudentGovernment changes its menu...

Trigger your club's pipeline. Until you do, the government-menu in your club's site will be out of date.

## If UpstreamTemplate changes...

NEEDS LINKS AND BETTER DESCRIPTION

```bash=
git clone PASTE_URL_TO_YOUR_CLUB_REPO
cd DIRECTORY_CREATED_BY_PREVIOUS_COMMAND
git switch -c synch-with-upstream
git remote new upstream PASTE_URL_TO_YOUR
git fetch upstream/main
git merge upstream/main
# If conflicts, make, test, stage, and commit resolutions.
git push origin synch-with-upstream
# Click link to create an MR, setting the target to your club's repo.
# Merge the MR.
cd ..
rm -rf DIRECTORY_CREATED_BY_FIRST_COMMAND
```

## Contributing a change to the UpstreamTemplate

NEEDS LINKS

1. Create an issue in your club's project.
2. From the issue, create a merge request.
3. From the merge request, open your files in the Web IDE.
4. Modify your files and commit your changes.
5. View your merge request, and when the jobs complete (refresh the page regularly), click on the "View app" link to view a demo of what your site will look like with the changes you've made.
6. When you are satisfied, mark the merge request as ready, and then merge it.
7. When the pipeline completes for main, your updated site has been published.
